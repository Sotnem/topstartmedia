class WelcomeController < ApplicationController
  def index
  end

  def add_client
    Client.create(name: params[:name], phone: params[:phone])
    redirect_to root_path(params)
  end

  # private

  # def client_params

  # end

end
